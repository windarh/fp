/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Praproses;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author wnd
 */
public class Stopword {
    public String[] stopwordsRemove (String[] input){
        String[] output = new String[input.length];
        try{
            String[] stopwords= {"di","dan","yang","apa","ada","0","1","2","3" ,"4" ,"5" ,"6" ,"7" ,"8" ,"9" ,"siapa","siapakah","dimana","dimanakah"
                                ,"kemana" ,"kemanakah" ,"darimana" ,"darimanakah" ,"kapan" ,"kapankah" ,"apa" ,"apakah" ,"berapa" 
                                ,"berapakah" ,"a" ,"ada" ,"adakalanya" ,"adalah" ,"adanya" ,"adapun" ,"agar" ,"akan" ,"akhir" ,"akhirnya" ,"aku" ,"biar" 
                                ,"anda" ,"andaikata" ,"antar" ,"antara"  ,"apabila" ,"apakah" ,"apalagi" ,"asal","asalkan" ,"atas" ,"atau"  ,"b" ,"bab" ,"maupun" ,"melainkan" 
                                ,"bagaikan" ,"bagaimana" ,"bagaimanakah" ,"bagaimanapun" ,"bagi" ,"bahkan" ,"bahwa"  ,"bawah" ,"mengapakah"
                                ,"beberapa" ,"begini" ,"begitu" ,"belakang" ,"belum" ,"berada" ,"berarti","berbagai" ,"berdasarkan" ,"berjalan" ,"berlangsung"  ,"hingga" 
                                ,"biasa" ,"biasanya" ,"bila" ,"bilamana" ,"bisa","bulan" ,"c" ,"d"  ,"dalam" ,"dan" ,"dapat" ,"dari" ,"daripada" ,"kamu" ,"karena" 
                                ,"demikian" ,"dengan","di" ,"dia" ,"diduga" ,"digunakan" ,"dimanapun" ,"dirinya" ,"disebut"  ,"dulu" ,"e" ,"f" ,"g","h" ,"hal" ,"hampir" 
                                ,"ingin" ,"ini" ,"inilah" ,"itu" ,"itu" ,"j" ,"jadi" ,"jangan" ,"jika" ,"jikalau" ,"juga"  ,"justru" ,"juta" ,"k","kalau" ,"kali" ,"kalian" 
                                ,"kedua" ,"kenapa" ,"kenapakah" ,"kepada","mungkin" ,"n" 
                                ,"keterangan" ,"ketika" ,"khusus" ,"kini" ,"kita" ,"l" ,"lagi" ,"lain" ,"lainnya" ,"lalu" ,"lama"  ,"katanya" ,"ke" ,"kecuali"
                                ,"m" ,"maka" ,"maksud" ,"mampu" ,"mana" ,"manakah" ,"mari" ,"masih" ,"masing-masing"  ,"mau" ,"selama" ,"selanjutnya"  
                                ,"memang"  ,"mengapa" ,"tentang" ,"tentu" 
                                ,"mereka" ,"merupakan" ,"meski" ,"meskipun" ,"misalnya"  
                                ,"namun","nanti" ,"o" ,"of" ,"oleh" ,"p" ,"pada" ,"padahal"  ,"para" ,"penting" ,"perlu" ,"pernah" ,"selain" ,"selalu" ,"nah"
                                 ,"pula" ,"pun" ,"punya" ,"q" ,"r"  ,"s" ,"saat"  ,"saja"  ,"sesuatu" 
                                ,"sampai" ,"sangat" ,"satu" ,"saya" ,"seakan-akan" ,"seandainya" ,"sebab" ,"sebagai" ,"sebagaimana" ,"sebagian" ,"sebanyak" ,"sebelum" ,"sebelumnya" ,"u" 
                                ,"sebenarnya"  ,"sebuah" ,"secara" ,"sedang" ,"sedangkan" ,"sedikit","segera" ,"sehingga" ,"sejak"  ,"sekali" ,"sekalipun" ,"sekarang" ,"sekitar" ,"menjadi","harus" 
                                ,"seluruh","semakin" ,"semenjak" ,"sementara" ,"sempat" ,"semua" ,"sendiri","senin" ,"seorang" ,"seperti" ,"seraya" ,"sering" ,"seringkali" ,"serta" ,"seseorang" ,"sesuai" ,"katanya" 
                                ,"setelah" ,"setiap" ,"suatu" ,"subbab" ,"sudah" ,"sumber" ,"sumber" ,"sungguh" ,"t" ,"tahu" ,"tak" ,"tampil" ,"tanggal" ,"tanpa" ,"tapi" ,"tatkala" ,"telah" ,"teman" ,"tengah" ,"ialah" 
                                ,"terhadap" ,"terkait" ,"terlalu" ,"terlihat" ,"termasuk" ,"ternyata" ,"tersebut" ,"terus" ,"terutama" ,"tetapi" ,"the" ,"tiap" ,"tidak" ,"tinggal" ,"tinggi" ,"tingkat"  
                                ,"untuk" ,"upaya" ,"usai" ,"utama" ,"utara" ,"v" ,"w" ,"walaupun" ,"wib" ,"x" ,"y" ,"ya" ,"yaitu" ,"yakni" ,"yang" ,"z","i" ,"ia" ,"hanya","kami","terdiri"};
            output = filterTitle(input, stopwords);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return output;
    }
    
    private String[] filterTitle(String[] input, String[] stopWords) {
        List<String> stopWordsList = Arrays.asList(stopWords);
        
        String[] hasil = new String[input.length];
        int j = 0;
        for (String input1 : input) {
            if (!stopWordsList.contains(input1)) {
                hasil[j] = input1;
                j++;
            }
        }
        
        return hasil;
    }

    
}
