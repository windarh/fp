/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import Algorithm.Filtering;
import Algorithm.Hashing;
import Algorithm.Kgram;
import Algorithm.Winnowing;
import Praproses.CaseFolding;
import Praproses.Stemmer;
import Praproses.Stopword;
import Praproses.Tokenizing;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

/**
 *
 * @author WINDA
 */
public class Document {
    
    private String judul;
    private String content;
    private String[] paragraf;
    
    private String[] kgramSeluruh;
    private int[] hashSeluruh;
    private String fingerprintSeluruh;
    
    private ArrayList<int []> hashParagraf = new ArrayList<int []>();
    private ArrayList<String> fingerprintParagraf = new ArrayList<String>();
    
    private String[] kgramSuspected;
    
    public Document(String filePath) {
        try {
            File file = new File(filePath);
            XWPFDocument doc = new XWPFDocument(new FileInputStream(file));
            XWPFWordExtractor extract = new XWPFWordExtractor(doc);
            
            this.judul = file.getName();
            this.content = extract.getText();
            
            this.setKgramSeluruh();
            this.setHashSeluruh();
            this.setFingerprintSeluruh();
            
            this.setParagraf(this.content.split("\n"));
            this.setHashFingerprintParagraf();
        } catch (IOException ex) {
            Logger.getLogger(Document.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Document(String filePath, String fingerprintPath) {
        try {
            File file = new File(filePath);
            XWPFDocument doc = new XWPFDocument(new FileInputStream(file));
            XWPFWordExtractor extract = new XWPFWordExtractor(doc);
            
            this.judul = file.getName();
            this.content = extract.getText();
            
            this.setKgramSeluruh();
            this.setHashSeluruh();
            this.setFingerprintSeluruh();
            //this.setFingerprintSeluruhFromFile(fingerprintPath);
            
            this.setParagraf(this.content.split("\n"));
            this.setHashFingerprintParagraf();
        } catch (IOException ex) {
            Logger.getLogger(Document.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    
    public String[] getParagraf() {
        return paragraf;
    }

    public void setParagraf(String[] paragraf) {
        this.paragraf = paragraf;
    }
    
    // method untuk buat kgram
    public String[] kgram(String text) {
        CaseFolding mycasefold = new CaseFolding();
        Tokenizing mytokens = new Tokenizing();
        Stopword stopword = new Stopword();
        Stemmer mystem = new Stemmer();
        Filtering myfilter = new Filtering();
        Kgram mykgram = new Kgram();

        String hasil_casefolding = mycasefold.casefolding(text);
        String[] hasil_tokenizing = mytokens.tokenizing(hasil_casefolding);
        String[] hasil_stopword = stopword.stopwordsRemove(hasil_tokenizing);
        String[] hasil_stemming = mystem.stemm(hasil_stopword);
        String hasil_space = myfilter.LihatHasilSpace(hasil_stemming);
        String[] hasil_kgram = mykgram.kgram(hasil_space, 5);
        
        return hasil_kgram;
    }
    
    // set kgram untuk keseluruhan dokumen
    public void setKgramSeluruh() {
        this.kgramSeluruh = this.kgram(this.content);
    }
    
    public String[] getKgramSeluruh() {
        return this.kgramSeluruh;
    }
    
    // method untuk buat hash
    public int[] hash(String text) {
        Hashing myhash = new Hashing();
        int[] hasil_hash = myhash.hashArray(this.kgram(text));
        return hasil_hash;
    }
    
    public void setHashSeluruh() {
        this.hashSeluruh = this.hash(this.content);
    }
    
    public int[] getHashSeluruh() {
        return hashSeluruh;
    }

    // method untuk buat fingerprint
    public String fingerprint(int[] hash) {
        Winnowing winnow = new Winnowing(hash);
        winnow.Bagi();
        return winnow.HasilWinnowing();
    }
    
    public String getFingerprintSeluruh() {
        return fingerprintSeluruh;
    }
    
    public void setFingerprintSeluruh() {
        this.fingerprintSeluruh = this.fingerprint(this.hashSeluruh);
    }
    
    public ArrayList<String> getFingerprintParagraf() {
        return fingerprintParagraf;
    }
    
    public void setFingerprintSeluruhFromFile(String filePath) {
        try {
            File fileFingerprint = new File(filePath);
            BufferedReader read = new BufferedReader(new FileReader(fileFingerprint));
            this.fingerprintSeluruh = "";
            String line = null;
            while((line = read.readLine()) != null) {
                this.fingerprintSeluruh += line + "\n";
            }
        }
        catch(IOException ex) {
            Logger.getLogger(Document.class.getName()).log(Level.SEVERE, null, ex);
        }
        //this.fingerprintSeluruh = this.fingerprint(this.hashSeluruh);
    }
    
    // setting up the hash and fingerprint for all paragraf
    public void setHashFingerprintParagraf() {       
        for(int i=0; i<this.paragraf.length; i++) {
            if(!this.paragraf[i].isEmpty()) {
                this.hashParagraf.add(this.hash(this.paragraf[i]));
                this.fingerprintParagraf.add(this.fingerprint(this.hashParagraf.get(this.hashParagraf.size() - 1)));
            }
        }
    }
    
    public void setKgramSuspected() {
        Winnowing winnow = new Winnowing(this.hashSeluruh);
        winnow.Bagi();
        ArrayList<int[]> windows = winnow.windowMin;
        
        String[] result = new String[windows.size()];
        for(int i=0; i<windows.size(); i++) {
            result[i] = this.kgramSeluruh[windows.get(i)[1]];
        }
        this.kgramSuspected = result;
    }
    
    public String[] getKgramSuspected() {
        return this.kgramSuspected;
    }
}
