/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithm;

/**
 *
 * @author wnd
 */
public class Hashing {
     public int hash(String input) {
        int hasil = 0;
        for(int i=0; i<input.length(); i++) {
            hasil += (int) input.charAt(i) * Math.pow(7, input.length() - i - 1);
            //System.out.println((int) input.charAt(i));
        }
        return hasil;
    }
    
    public int[] hashArray(String[] input) {
        int[] hasil = new int[input.length];
        for(int j=0; j<input.length; j++) {
            for(int i=0; i<input[j].length(); i++) {
                hasil[j] += (int) input[j].charAt(i) * Math.pow(7, input[j].length() - i - 1);
            }
        }
        return hasil;
    }
    
    /*public String lihatHasilHash(int[] input) {
        String temp = "";
        for(int i=0; i<input.length; i++) {
            temp += String.valueOf(input[i]) + "\n ";
        }
        return temp;
    }*/
    
}
