/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Controller.FPWControl;
import Entity.Document;
import java.awt.Component;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author WINDA
 */
public class OneToMany extends javax.swing.JFrame {

    Document suspectedDoc;
    ArrayList<Document> originalDocuments = new ArrayList<Document>();
    
    /**
     * Creates new form OneToMany
     */
    public OneToMany() {
        initComponents();
        
        this.setLocationRelativeTo(null); // supaya muncul di tengah
        
        // get all documents from database
        originalDocuments.add(new Document("data/documents/Amanah Asli.docx", "data/documents/fingerprint - Amanah Asli.docx.txt"));
        originalDocuments.add(new Document("data/documents/Dana Asli.docx", "data/documents/fingerprint - Dana Asli.docx.txt"));
        originalDocuments.add(new Document("data/documents/Dini Asli.docx", "data/documents/fingerprint - Dini Asli.docx.txt"));
        originalDocuments.add(new Document("data/documents/Faisal Asli.docx", "data/documents/fingerprint - Faisal Asli.docx.txt"));
        originalDocuments.add(new Document("data/documents/Tri Asli.docx", "data/documents/fingerprint - Tri Asli.docx.txt"));
        
        jPanelKeseluruhan.setVisible(false);
        jPanelPerParagraf.setVisible(false);
        
        jTableKeseluruhan.setDefaultRenderer(Object.class, new LastRowBold());
        jTablePerParagraf.setDefaultRenderer(Object.class, new LastRowBold());
    }
    
    class LastRowBold extends DefaultTableCellRenderer {
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            JLabel parent = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if(row == table.getRowCount()-1) {
                parent.setFont(parent.getFont().deriveFont(Font.BOLD));
            }
            return parent;
        }    
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelTitle = new javax.swing.JLabel();
        jLabelSubtitle = new javax.swing.JLabel();
        jTextFieldDocument1 = new javax.swing.JTextField();
        jButtonBrowseDocument1 = new javax.swing.JButton();
        jButtonCompare = new javax.swing.JButton();
        jPanelKeseluruhan = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableKeseluruhan = new javax.swing.JTable();
        jPanelPerParagraf = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTablePerParagraf = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Plagiarism Detection: Document to Database");
        setMinimumSize(new java.awt.Dimension(480, 620));
        setResizable(false);

        jLabelTitle.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabelTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelTitle.setText("Perbandingan Dokumen & Database");

        jLabelSubtitle.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabelSubtitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelSubtitle.setText("One-to-Many");

        jTextFieldDocument1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldDocument1ActionPerformed(evt);
            }
        });

        jButtonBrowseDocument1.setText("Pilih Dokumen");
        jButtonBrowseDocument1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBrowseDocument1ActionPerformed(evt);
            }
        });

        jButtonCompare.setText("Bandingkan");
        jButtonCompare.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCompareActionPerformed(evt);
            }
        });

        jPanelKeseluruhan.setBorder(javax.swing.BorderFactory.createTitledBorder("Perbandingan Secara Keseluruhan"));
        jPanelKeseluruhan.setMaximumSize(new java.awt.Dimension(460, 200));
        jPanelKeseluruhan.setMinimumSize(new java.awt.Dimension(460, 200));
        jPanelKeseluruhan.setPreferredSize(new java.awt.Dimension(460, 200));

        jTableKeseluruhan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Dokumen", "Status", "Persentase"
            }
        ));
        jScrollPane1.setViewportView(jTableKeseluruhan);

        javax.swing.GroupLayout jPanelKeseluruhanLayout = new javax.swing.GroupLayout(jPanelKeseluruhan);
        jPanelKeseluruhan.setLayout(jPanelKeseluruhanLayout);
        jPanelKeseluruhanLayout.setHorizontalGroup(
            jPanelKeseluruhanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        jPanelKeseluruhanLayout.setVerticalGroup(
            jPanelKeseluruhanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
        );

        jPanelPerParagraf.setBorder(javax.swing.BorderFactory.createTitledBorder("Perbandingan Secara Per Paragraf"));
        jPanelPerParagraf.setMaximumSize(new java.awt.Dimension(460, 200));
        jPanelPerParagraf.setMinimumSize(new java.awt.Dimension(460, 200));
        jPanelPerParagraf.setPreferredSize(new java.awt.Dimension(460, 200));

        jTablePerParagraf.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Dokumen", "Status", "Persentase"
            }
        ));
        jScrollPane2.setViewportView(jTablePerParagraf);

        javax.swing.GroupLayout jPanelPerParagrafLayout = new javax.swing.GroupLayout(jPanelPerParagraf);
        jPanelPerParagraf.setLayout(jPanelPerParagrafLayout);
        jPanelPerParagrafLayout.setHorizontalGroup(
            jPanelPerParagrafLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        jPanelPerParagrafLayout.setVerticalGroup(
            jPanelPerParagrafLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelSubtitle, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabelTitle, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
                    .addComponent(jButtonCompare, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jTextFieldDocument1, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonBrowseDocument1))
                    .addComponent(jPanelKeseluruhan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelPerParagraf, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelSubtitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldDocument1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonBrowseDocument1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonCompare, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanelKeseluruhan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanelPerParagraf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonBrowseDocument1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonBrowseDocument1ActionPerformed
        // TODO add your handling code here:
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Word Documents", "docx");
        chooser.setFileFilter(filter);
        chooser.showOpenDialog(null);
        String path = chooser.getSelectedFile().getAbsolutePath();
        jTextFieldDocument1.setText(path);
    }//GEN-LAST:event_jButtonBrowseDocument1ActionPerformed

    private void jButtonCompareActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCompareActionPerformed
        // TODO add your handling code here:
        double jumlahpersentase = 0;
        double totalpersentase = 0;
        
        if(!jTextFieldDocument1.getText().isEmpty()) {
            FPWControl fpwControl = new FPWControl();
            
            suspectedDoc = new Document(jTextFieldDocument1.getText());
            
            double[] resultsKeseluruhan = fpwControl.compareOneToManyKeseluruhan(suspectedDoc, originalDocuments);
            
            ArrayList<double[][]> resultsPerParagraf = fpwControl.compareOneToManyPerParagraf(suspectedDoc, originalDocuments);
            double[] persentasePerParagraf = fpwControl.persentaseOneToManyPerParagraf(suspectedDoc, originalDocuments, resultsPerParagraf);
                        
            jPanelKeseluruhan.setVisible(true);
            jPanelPerParagraf.setVisible(true);

            String header[] = new String[] {"Dokumen", "Status", "Persentase"};
            
            // table keseluruhan
            DefaultTableModel dmKeseluruhan = new DefaultTableModel() {
                @Override
                public boolean isCellEditable(int row, int column) { return false; }
            };
            dmKeseluruhan.setColumnIdentifiers(header);
            jTableKeseluruhan.setModel(dmKeseluruhan);
            
            for(int i=0; i<originalDocuments.size(); i++) {
                String status = "";
                if(resultsKeseluruhan[i] >= 30) status = "Plagiat";
                else status = "Tidak Plagiat";
                String persentase = String.format("%.1f%n", resultsKeseluruhan[i]) + "%";
                
                Object[] obj = {originalDocuments.get(i).getJudul(), status, persentase};
                dmKeseluruhan.addRow(obj);
                
                totalpersentase = totalpersentase + resultsKeseluruhan[i];
            }  
            String kesimpulan = "";
            String totalpersen = String.format("%.1f%n", totalpersentase) + "%"; 
            if(totalpersentase >= 30.0) kesimpulan = "Plagiat";
            else kesimpulan = "Tidak Plagiat";
            Object[] obj = {"Total", kesimpulan, totalpersen};           
            dmKeseluruhan.addRow(obj);
            
            

            jTableKeseluruhan.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent me) {
                    JTable table =(JTable) me.getSource();
                    Point p = me.getPoint();
                    int row = table.rowAtPoint(p);
                    if (me.getClickCount() == 2) {
                        fpwControl.runDetails(suspectedDoc, originalDocuments.get(row), resultsKeseluruhan[row]);
                    }
                }
            });
            
            // table per paragraf
            DefaultTableModel dmPerParagraf = new DefaultTableModel() {
                @Override
                public boolean isCellEditable(int row, int column) { return false; }
            };
            dmPerParagraf.setColumnIdentifiers(header);
            jTablePerParagraf.setModel(dmPerParagraf);
            
            for(int i=0; i<originalDocuments.size(); i++) {
                String status = "";
                        
                if(persentasePerParagraf[i] >= 30) status = "Plagiat";
                else status = "Tidak Plagiat";
                String persentase = String.format("%.1f%n", persentasePerParagraf[i]) + "%";
                
                Object[] obt = {originalDocuments.get(i).getJudul(), status, persentase};
                dmPerParagraf.addRow(obt);
                
                jumlahpersentase = jumlahpersentase + persentasePerParagraf[i];
            }
            
            String simpulanStatus = "";
            String jumpersentase = String.format("%.1f%n", jumlahpersentase) + "%"; 
            if(jumlahpersentase >= 30.0) simpulanStatus = "Plagiat";
            else simpulanStatus = "Tidak Plagiat";      
            Object[] obt = {"Total", simpulanStatus, jumpersentase};
            dmPerParagraf.addRow(obt);
            
            jTablePerParagraf.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent me) {
                    JTable table =(JTable) me.getSource();
                    Point p = me.getPoint();
                    int row = table.rowAtPoint(p);
                    if (me.getClickCount() == 2) {
                        fpwControl.runDetails(suspectedDoc, originalDocuments.get(row), persentasePerParagraf[row], resultsPerParagraf.get(row));
                    }
                }
            });
        }
        else {
            JOptionPane.showMessageDialog(this, "Harap pilih dokumen terlebih dahulu!", "Perhatian", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButtonCompareActionPerformed

    private void jTextFieldDocument1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldDocument1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldDocument1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(OneToMany.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(OneToMany.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(OneToMany.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(OneToMany.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new OneToMany().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonBrowseDocument1;
    private javax.swing.JButton jButtonCompare;
    private javax.swing.JLabel jLabelSubtitle;
    private javax.swing.JLabel jLabelTitle;
    private javax.swing.JPanel jPanelKeseluruhan;
    private javax.swing.JPanel jPanelPerParagraf;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTableKeseluruhan;
    private javax.swing.JTable jTablePerParagraf;
    private javax.swing.JTextField jTextFieldDocument1;
    // End of variables declaration//GEN-END:variables
}
